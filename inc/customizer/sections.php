<?php
/**
 * Customizer sections.
 *
 * @package foodwage
 */

/**
 * Register the section sections.
 *
 * @author WDS
 * @param object $wp_customize Instance of WP_Customize_Class.
 */
function fwr_customize_sections( $wp_customize ) {

	// Register additional scripts section.
	$wp_customize->add_section(
		'fwr_additional_scripts_section',
		array(
			'title'    => esc_html__( 'Additional Scripts', 'foodwage' ),
			'priority' => 10,
			'panel'    => 'site-options',
		)
	);

	// Register a social links section.
	$wp_customize->add_section(
		'fwr_social_links_section',
		array(
			'title'       => esc_html__( 'Social Media', 'foodwage' ),
			'description' => esc_html__( 'Links here power the display_social_network_links() template tag.', 'foodwage' ),
			'priority'    => 90,
			'panel'       => 'site-options',
		)
	);

	// Register a header section.
	$wp_customize->add_section(
		'fwr_header_section',
		array(
			'title'    => esc_html__( 'Header Customizations', 'foodwage' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);

	// Register a footer section.
	$wp_customize->add_section(
		'fwr_footer_section',
		array(
			'title'    => esc_html__( 'Footer Customizations', 'foodwage' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);
}
add_action( 'customize_register', 'fwr_customize_sections' );
