<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package foodwage
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
