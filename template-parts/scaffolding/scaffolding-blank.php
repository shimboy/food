<?php
/**
 * The template used for displaying X in the scaffolding library.
 *
 * @package foodwage
 */

?>

<section class="section-scaffolding">
	<h2 class="scaffolding-heading"><?php esc_html_e( 'X', 'foodwage' ); ?></h2>
</section>
