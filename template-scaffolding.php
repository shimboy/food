<?php
/**
 * Template Name: Scaffolding
 *
 * Template Post Type: page, scaffolding, fwr_scaffolding
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package foodwage
 */

get_header(); ?>

	<main id="main" class="site-main container">

		<?php do_action( 'fwr_scaffolding_content' ); ?>

	</main><!-- #main -->

<?php get_footer(); ?>
